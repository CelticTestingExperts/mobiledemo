﻿/*
 * Created by Ranorex
 * User: CTELaptop1064
 * Date: 9/26/2016
 * Time: 8:28 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;
using System.Diagnostics;
using System.Data.SqlClient;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace BackBaseMobile.CodeModules
{
    /// <summary>
    /// Description of DemoLibary.
    /// </summary>
    [TestModule("407937EE-ED2D-4C9F-9A70-1E5CD1CD17F2", ModuleType.UserCode, 1)]
    public class DemoLibary : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public DemoLibary()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            
            
        }
        
        public static void DoThing()
        {
        	//Do the thing
        }
        
        public static string strString()
        {
        	return "Thing";
        }
        
        public static int intNumber()
        {
        	return 1;
        }
        
        // ===============================
// PURPOSE: Constructing a DataTable from an SQL query
// SPECIAL NOTES: 
// ===============================  
	public static DataTable SQLQueryCreateDataTable(string strQuery, string strServer, string strDatabase)
	{
		string strConnection = @"Server=" + strServer + ";Database=" + strDatabase + ";Trusted_Connection=False;MultipleActiveResultSets=true;UID=autobuild;PWD=abc123";
	
		using (SqlConnection con = new SqlConnection(strConnection))
		{
			SqlDataAdapter dap = new SqlDataAdapter(strQuery,con);
			DataTable dt = new DataTable();
			dap.Fill(dt);
			return dt;
		}
	}
// ===============================
// PURPOSE: Update a product qty value in ILS 
// SPECIAL NOTES:
// ===============================    
        
 		public static void SQLQueryUpdate(string strQuery, string strServer, string strDatabase)
        {
        	//Create Connection string
        	
        	string strConnection = @"Server=" + strServer + ";Database=" + strDatabase + ";Trusted_Connection=False;MultipleActiveResultSets=true;UID=autobuild;PWD=abc123"; 
        	
        	//Create SQL connection
            SqlConnection conn = new SqlConnection(strConnection);
            
            //Open connection
            conn.Open();
            
            //Create command with parameters for sql query and connection string
            SqlCommand com = new SqlCommand(strQuery, conn);
            
            //store value returned from query
            object objQuery = com.ExecuteScalar();
        }
 		
 		
 // ===============================
// PURPOSE: Returns a value from a database. 
// SPECIAL NOTES:
// ===============================    
        
        
 		public static string SQLQueryReturn(string strQuery, string strServer, string strDatabase)
        {
        	//Create Connection string
        	
        	string strConnection = @"Server=" + strServer + ";Database=" + strDatabase + ";Trusted_Connection=False;MultipleActiveResultSets=true;UID=autobuild;PWD=abc123";
        	
        	//Create SQL connection
            SqlConnection conn = new SqlConnection(strConnection);
            
            //Open connection
            conn.Open();
            
            //Create command with parameters for sql query and connection string
            SqlCommand com = new SqlCommand(strQuery, conn);
            
            //store value returned from query
            object objQuery = com.ExecuteScalar();
            //Delay.Seconds(2);
            string results = objQuery.ToString();

            //Return Results
            return results;
        	
        }
 				
 		
    }
}


